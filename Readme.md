# ExceptionlessDemo
Exceptionless日志系统的使用Demo

> 安装组件
```c#
//与AspNetCode集成
Install-Package Exceptionless.AspNetCore
//与Nlog集成
Install-Package NLog.Web.AspNetCore
Install-Package Exceptionless.NLog
```