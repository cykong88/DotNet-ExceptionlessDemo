﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exceptionless;
using Exceptionless.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Ynzp.Core.Exceptions;

namespace ExceptionlessDemo.Controllers
{
    [Route("log")]
    [ApiController]
    public class LogController : ControllerBase
    {
        private ILogger logger;

        public LogController(ILogger<LogController> logger)
        {
            this.logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            //try
            {
                //记录信息
                logger.LogInformation("这是一个普通日志记录code:{12345678999}");

                //记录异常日志
                logger.LogError("这是一个错误日志记录");
                logger.LogWarning("这是一个警告日志记录");

                //抛出异常
                throw new Exception($"主动抛出的错误");
                return "OK";
            }
/*            catch(Exception ex)
            {
                //Exceptionless有防止重复提交功能，所以
                //这两个目前只会有一个写入到Exceptionless

                //ex的扩展方法
                ex.ToExceptionless().Submit();

                //记录异常日志
                logger.LogError(ex, "logger--" + ex.Message);

                return "Err";
            }*/
        }

        [HttpPost("post")]
        public string DoPost([FromBody]InputModel inputModel)
        {
            var name = inputModel.Name;

            throw new BizException($"Post抛出的错误");

            return "OK";
        }

        [HttpPost("upload")]
        public string UploadFiles()
        {
            var files = Request.Form.Files;

            throw new BizException($"UploadFiles抛出的错误");

            return "OK";
        }
    }

    public class InputModel
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
}