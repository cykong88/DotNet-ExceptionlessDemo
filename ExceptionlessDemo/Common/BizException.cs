﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Ynzp.Core.AspNet;


namespace Ynzp.Core.Exceptions
{
    /// <summary>
    /// 业务异常
    /// </summary>
    public class BizException : Exception
    {
        /// <summary>
        /// 错误码
        /// </summary>
        public int ErrCode { get; set; }
        /// <summary>
        /// 用来显示的消息
        /// </summary>
        public string ShowMsg { get; set; }

        public object ErrData { get; set; }

        //
        // 摘要:
        //     Initializes a new instance of the System.Exception class.
        public BizException()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        /// <param name="errCode"></param>
        /// <param name="showMsg"></param>
        protected BizException(SerializationInfo info, StreamingContext context, int errCode = 0, string showMsg = "", object errData = null) : base(info, context)

        {
            this.ErrCode = errCode;
            this.ShowMsg = showMsg;
            this.ErrData = errData;
        }

        //
        // 摘要:
        //     Initializes a new instance of the System.Exception class with a specified error
        //     message.
        //
        // 参数:
        //   message:
        //     The message that describes the error.
        public BizException(string message, int errCode = 0, string showMsg = "",object errData = null) : base(message)
        {
            this.ErrCode = errCode;
            this.ShowMsg = showMsg;
            this.ErrData = errData;
        }

        //
        // 摘要:
        //     Initializes a new instance of the System.Exception class with a specified error
        //     message and a reference to the inner exception that is the cause of this exception.
        //
        // 参数:
        //   message:
        //     The error message that explains the reason for the exception.
        //
        //   innerException:
        //     The exception that is the cause of the current exception, or a null reference
        //     (Nothing in Visual Basic) if no inner exception is specified.
        public BizException(string message, Exception innerException, int errCode = 0, string showMsg = "", object errData = null) : base(message, innerException)
        {
            this.ErrCode = errCode;
            this.ShowMsg = showMsg;
            this.ErrData = errData;
        }

        public BizException(int errCode = 0, string showMsg = "", object errData = null) : base(showMsg)
        {
            this.ErrCode = errCode;
            this.ShowMsg = showMsg;
            this.ErrData = errData;
        }

/*        public BizException(ResultCodes resultCode) : base(resultCode.GetDescription())
        {
            this.ErrCode = resultCode.ToInt32();
            this.ShowMsg = resultCode.GetDescription();
        }

        public BizException(EnumCodeMsg codeMsg) : base(codeMsg.Description)
        {
            this.ErrCode = codeMsg.Code;
            this.ShowMsg = codeMsg.Description;
        }


        public BizException(string message, EnumCodeMsg codeMsg) : base(message)
        {
            this.ErrCode = codeMsg.Code;
            this.ShowMsg = codeMsg.Description;
        }*/
    }
}
