﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace System
{
    public static class EnumExtensions
    {
        /// <summary>
        /// 得到枚举DescriptionAttribute文本
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum item)
        {
            string text = item.ToString();
            return (item.GetType().GetField(text)?.GetCustomAttributes(typeof(DescriptionAttribute), inherit: false)?.FirstOrDefault() as DescriptionAttribute)?.Description ?? text;
        }

        public static long ToInt64(this Enum item)
        {
            return Convert.ToInt64(item);
        }

        public static int ToInt32(this Enum item)
        {
            return Convert.ToInt32(item);
        }

/*        public static EnumCodeMsg ToEnumCodeMsg(this Enum item)
        {
            return new EnumCodeMsg()
            {
                Code = Convert.ToInt32(item),
                Value = item.ToString(),
                Description = item.GetDescription()
            };
        }*/
    }
}
