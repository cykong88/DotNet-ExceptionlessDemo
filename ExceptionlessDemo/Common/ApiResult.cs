﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ynzp.Core.AspNet
{
    /// <summary>
    /// 输出数据接口
    /// </summary>
    public interface IApiResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        [JsonIgnore]
        bool success { get; }

        /// <summary>
        /// 状态码
        /// </summary>
        int code { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        string msg { get; }
    }

    /// <summary>
    /// 响应数据泛型接口
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IApiResult<T> : IApiResult
    {
        /// <summary>
        /// 返回数据
        /// </summary>
        T data { get; }
    }

    /// <summary>
    /// 输出数据
    /// </summary>
    public class ApiResult<T> : IApiResult<T>
    {
        /// <summary>
        /// 是否成功标记
        /// </summary>
        [JsonIgnore]
        public bool success { get; private set; }

        /// <summary>
        /// 状态码
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 消息
        /// </summary>
        public string msg { get; private set; }

        /// <summary>
        /// 数据
        /// </summary>
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public T data { get; private set; }

        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="msg">消息</param>
        public ApiResult<T> Ok(string msg = "")
        {
            this.success = true;
            this.code = success ? 1 : 0;
            this.data = default(T);
            this.msg = msg;

            return this;
        }

        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="msg">消息</param>
        public ApiResult<T> Ok(T data, string msg = "")
        {
            this.success = true;
            this.code = success ? 1 : 0;
            this.data = data;
            this.msg = msg;

            return this;
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public ApiResult<T> NotOk(string msg = "", T data = default(T))
        {
            this.success = false;
            this.code = 0;
            this.msg = msg;
            this.data = data;

            return this;
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="code">错误代码</param>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public ApiResult<T> NotOk(int code = 0, string msg = "", T data = default(T))
        {
            this.success = false;
            this.code = code;
            this.msg = msg;
            this.data = data;

            return this;
        }
    }

    /// <summary>
    /// 输出数据静态类
    /// </summary>
    public static partial class ApiResult
    {

        /// <summary>
        /// 成功
        /// </summary>
        /// <returns></returns>
        public static IApiResult Ok()
        {
            return new ApiResult<string>().Ok(null, string.Empty);
        }

        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static IApiResult Ok(string msg = "")
        {
            return new ApiResult<string>().Ok(null, msg);
        }

        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data">数据</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static IApiResult Ok<T>(T data = default(T), string msg = "")
        {
            return new ApiResult<T>().Ok(data, msg);
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="code">错误代码</param>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public static IApiResult NotOk<T>(int code = 0, string msg = "", T data = default(T))
        {
            return new ApiResult<T>().NotOk(code, msg, data);
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public static IApiResult NotOk<T>(string msg = "", T data = default(T))
        {
            return new ApiResult<T>().NotOk(msg, data);
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="code">错误代码</param>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static IApiResult NotOk(int code = 0, string msg = "")
        {
            return new ApiResult<string>().NotOk(code, msg);
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg">消息</param>
        /// <returns></returns>
        public static IApiResult NotOk(string msg = "")
        {
            return new ApiResult<string>().NotOk(msg);
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="enumCodeMsg">错误</param>
        /// <returns></returns>
/*        public static IApiResult NotOk(EnumCodeMsg enumCodeMsg)
        {
            return new ApiResult<string>().NotOk(enumCodeMsg.Code, enumCodeMsg.Description, null);
        }*/


        /// <summary>
        /// 返回内容
        /// </summary>
        /// <param name="code">错误代码</param>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public static IApiResult Result<T>(int code, string msg = "", T data = default(T))
        {
            return new ApiResult<T>().NotOk(code, msg, data);
        }

        /// <summary>
        /// 返回内容
        /// </summary>
        /// <param name="code">错误代码</param>
        /// <param name="msg">消息</param>
        /// <param name="data">数据</param>
        /// <returns></returns>
        public static IApiResult Result(int code, string msg = "")
        {
            return new ApiResult<string>().NotOk(code, msg, null);
        }

        /// <summary>
        /// 返回内容
        /// </summary>
        /// <param name="enumCodeMsg">错误</param>
        /// <returns></returns>
/*        public static IApiResult Result(EnumCodeMsg enumCodeMsg)
        {
            return new ApiResult<string>().NotOk(enumCodeMsg.Code, enumCodeMsg.Description, null);
        }*/
    }
}
