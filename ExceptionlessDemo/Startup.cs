using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exceptionless;
using ExceptionlessDemo.Filter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Config;
using Ynzp.DemoApi.Filters;

namespace ExceptionlessDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(options =>
            {
                //全局异常处理Filter
                //options.Filters.Add(typeof(GlobalExceptionFilter));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //*********使用Exeptionless中间件，需要组件Exceptionless.AspNetCore
            //从配置文件Exceptionless节自动读取配置
            //或手动设置
            //ExceptionlessClient.Default.Configuration.ApiKey = Configuration["Exceptionless:ApiKey"];
            //ExceptionlessClient.Default.Configuration.ServerUrl = Configuration["Exceptionless:ServerUrl"];
            //默认使用ExceptionlessClient.Default来输出数据，或Exception.ToExceptionless().Submit();来输出异常
            //ExceptionlessClient.Default.me
            app.UseExceptionless(Configuration);

            //注册全局异常处理中间件
            app.UseMiddleware<GlobalExceptionMiddleware>();

            app.UseHttpsRedirection();

            //***********很重要
            //如果不这样处理,Asp.Net Core 3.1 获取不到Post、Put请求的内容
            app.Use((context, next) =>
            {
                context.Request.EnableBuffering();
                return next();
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
