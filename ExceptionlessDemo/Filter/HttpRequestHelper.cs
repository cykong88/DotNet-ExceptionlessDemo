﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionlessDemo.Filter
{
    public class HttpRequestHelper
    {
        /// <summary>
        /// 异步读取请求Body内容
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static async Task<string> ReadBodyAsync(HttpRequest request, bool logUpload = false)
        {
            string body = "";
            if (request.ContentLength > 0)
            {
                var isUpLoad = IsUploadFile(request);
                if (!isUpLoad || (isUpLoad && logUpload))
                {
                    //允许Request反复读取
                    request.EnableBuffering();
                    using (var reader = new StreamReader(request.Body, GetRequestEncoding(request)))
                    {
                        request.Body.Position = 0;// .Seek(0, SeekOrigin.Begin);
                        body = await reader.ReadToEndAsync();
                        request.Body.Position = 0;
                        //request.Body.Seek(0, SeekOrigin.Begin);
                    }
                }

            }
            return body;
        }

        /// <summary>
        /// 判断是否是上传文件
        /// </summary>
        /// <param name="request"></param>
        public static bool IsUploadFile(HttpRequest request)
        {
            try
            {
                if (request.Form?.Files?.Count > 0)
                    return true;
                else
                    return false;
            }
            catch(Exception ex)
            {
                return false;
            }

            if (request.ContentType.StartsWith(ContentTypeConst.FormDataUpload)
                || request.ContentType.StartsWith(ContentTypeConst.FormDataBinary))
                return true;
            else
                return false;
        }

        /// <summary>
        /// 得到请求的字符集
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Encoding GetRequestEncoding(HttpRequest request)
        {
            var requestContentType = request.ContentType;
            var requestMediaType = requestContentType == null ? default(MediaType) : new MediaType(requestContentType);
            var requestEncoding = requestMediaType.Encoding;
            if (requestEncoding == null)
            {
                requestEncoding = Encoding.UTF8;
            }
            return requestEncoding;
        }
    }

    public class ContentTypeConst
    {
        public const string Json = "application/json";
        public const string Xml = "application/xml";
        public const string Text = "text/plain";
        public const string Javascript = "application/javascript";

        public const string FormData = "multipart/form-data";
        public const string FormDataUpload = "x-www-form-urlencoded";
        public const string FormDataBinary = "application/octet-stream";
    }
}
