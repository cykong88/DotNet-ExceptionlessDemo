#***************
#1. 安装Docker
#设置yum源
sudo yum -y install yum-utils
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
#yum包更新到最新
sudo yum update -y

#安装Docker最新版
sudo yum install docker-ce -y
#设置Docker自启动
sudo systemctl enable  docker
#启动Docker
sudo systemctl start docker

#配置国内镜像 /etc/docker/daemon.json
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://i9it9b0k.mirror.aliyuncs.com"]
}
EOF
#加载配置文件,ReStart
sudo systemctl daemon-reload
sudo systemctl restart docker

#安装docker-compose,最新版本需要手动查询一下
sudo curl -L https://get.daocloud.io/docker/compose/releases/download/1.25.5/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#*******************************
#2. 其它一些工具的安装
#安装rz和sz命令
sudo yum -y install lrzsz

#安装防火墙